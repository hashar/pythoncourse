#! /usr/bin/env python
# -*- coding: utf-8 -*-


import nbformat


FNAME = "basics.ipynb"

# HIDE_TAGS = set(["exercise-solution"])
HIDE_TAG = "exercise-solution"


def getTags(cell):
    try:
        return cell.metadata.tags
    except AttributeError:
        return ()


def clearOutput(nb):
    for cell in nb.cells:
        if cell.cell_type == "code":
            cell.outputs == []
        if HIDE_TAG in getTags(cell):
            try:
                cell.metadata.slideshow.slide_type = "skip"
            except:
                cell.metadata["slideshow"] = {"slide_type": "skip"}
    return nb


if __name__ == "__main__":

    fname = FNAME
    with open(fname, "r") as f:
        nb = nbformat.read(f, nbformat.NO_CONVERT)
    nb_clean = clearOutput(nb)

    with open(FNAME, "w") as f:
        nb = nbformat.write(nb_clean, f, nbformat.NO_CONVERT)
